<?php
require_once('./animal.php');
require_once('./Frog.php');
require_once('./Ape.php');

// Release 0
$sheep = new Animal("shaun");

echo "Name : ". $sheep->name . "<br>";
echo "Legs : " . $sheep->legs. "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded. "<br><br>";

// Release 1
$kodok = new Frog("buduk");
echo "Name : " .$kodok->name. "<br>";
echo "Legs : " .$kodok->legs. "<br>";
echo "Cold Blooded : " .$kodok->cold_blooded. "<br>";
echo "Jump : ";
$kodok->jump();
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : " .$sungokong->name. "<br>";
echo "Legs : " .$sungokong->legs. "<br>";
echo "Cold Blooded : " .$sungokong->cold_blooded. "<br>";
echo "Yell : ";
$sungokong->yell();



?>