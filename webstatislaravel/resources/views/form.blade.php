<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post" id="signup">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" id="fname" name="fname"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" id="lname" name="lname"><br>
    </form>

    <p>Gender:</p>
    <form>
        <input type="radio" id="male" name="gender" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="Female">
        <label for="female">Female</label><br>
        <input type="radio" id="others" name="gender" value="Others">
        <label for="others">Others</label><br>
    </form>

    <p>Nationality:</p>
    <form>
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select>
    </form>

    <p>Languange Spoken:</p>
    <form>
        <input type="checkbox" id="indonesia" name="indonesia" value="Bahasa Indonesia">
        <label for="indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="english" value="English">
        <label for="english">English</label><br>
        <input type="checkbox" id="others" name="others" value="Others">
        <label for="others">Others</label><br>
    </form>

    <p>Bio:</p>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up" form="signup">
</body>
</html>
