@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Create New Cast</h3>
        </div>


            <form role="form" action="/casts" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', '') }}" placeholder="Enter Name">
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Age</label>
                        <input type="text" class="form-control" id="age" name="age" value="{{ old('age', '') }}" placeholder="Password">
                        @error('age')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Bio</label>
                        <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', '') }}" placeholder="Password">
                        @error('bio')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
    </div>
</div>

@endsection
