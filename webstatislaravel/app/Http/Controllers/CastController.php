<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view('casts.create');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('casts')->insert([
            "name" => $request["name"],
            "age" => $request["age"],
            "bio" => $request["bio"]
        ]);

        return redirect('/casts')->with('success', 'Cast Berhasil Disimpan!');
    }

    public function index(){
        $casts = DB::table('casts')->get();
        return view('casts.index', compact('casts'));
    }

    public function show($id){
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('casts.show', compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('casts')->where('id', $id)->first();

        return view('casts.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('casts')
                     ->where('id', $id)
                     ->update([
                         'name' => $request['name'],
                         'age' => $request['age'],
                         'bio' => $request['bio']
                     ]);
        return redirect('/casts')->with('success', 'Berhasil update cast!');
    }

    public function destroy($id){
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/casts')->with('success', 'Cast berhasil dihapus!');
    }

}
